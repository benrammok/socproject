﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace UDPSoc
{
    public partial class Form1 : Form
    {
        private Thread workThread;
        private string udpMessage;
        
        private void enableFreq() {
            textBox1.Enabled = true;
            trackBar1.Enabled = true;
            textBox2.Enabled = true;
        }

        private void disableFreq()
        {
            textBox1.Enabled = false;
            trackBar1.Enabled = false;
            textBox2.Enabled = false;
        }



        private void updateForm(Object sender, EventArgs e) {
            string[] commands = udpMessage.Split(',');

            if (commands[0] == "$WARN") {
                if (commands[1] == "$TEMP\r\n") {
                    label1.Visible = true;
                }
            }

            if (commands[0] == "$FAULT") {
                richTextBox1.Text += "FAULT Detected\r\n";
            }

            if (commands[0] == "$EMPTY\r\n")
            {
                richTextBox1.Text += "Trap has been emptied\r\n";
                label1.Visible = false;
            }

            if (commands[0] == "$RESET\r\n") {
                    richTextBox1.Text += "SYSTEM RESET\r\n";
                    label1.Visible = false;
            }

            if (commands[0] == "$TEMP") {
                if (commands[1] != "$DOGN")
                {
                    label5.Text = commands[1];
                }
                else {
                    label9.Text = commands[2];
                }
            }

            if (commands[0] == "$NUM")
            {
                label7.Text = commands[1];
            }

            if (commands[0] == "$FREQ") {
                if (commands[1] == "KILL\r\n") {
                    comboBox1.SelectedIndex = 0;
                    disableFreq();
                }
                if (commands[1] == "STUN\r\n")
                {
                    comboBox1.SelectedIndex = 1;
                    disableFreq();
                }
                if (commands[1] == "ADJUST")
                {
                    enableFreq();
                    comboBox1.SelectedIndex = 2;
                    if(int.Parse(commands[2]) >= 10){
                        trackBar1.Value = int.Parse(commands[2]);
                        textBox1.Text = trackBar1.Value.ToString();
                    }
                    if (int.Parse(commands[3]) > 0) {
                        textBox2.Text = commands[3].ToString();
                    }
                }
            }
        }

        private void recieveUDP() {
            IPEndPoint iPEnd = new IPEndPoint(IPAddress.Any,0);
            UdpClient sock = new UdpClient(9050);

            byte[] data = new byte[1024];
            
            while (true)
            {
                try
                {
                    data = sock.Receive(ref iPEnd);
                    udpMessage = ASCIIEncoding.ASCII.GetString(data, 0, data.Length);
                    Invoke(new EventHandler(updateForm));
                }
                catch (Exception e)
                {

                }
            }
            sock.Close();
            
        }

        private void sendUDP(string UDPMESSAGE) {
            IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Parse("128.39.112.112"), 9050);
            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            byte[] msg = ASCIIEncoding.ASCII.GetBytes(UDPMESSAGE);
            sock.SendTo(msg, SocketFlags.None, iPEndPoint);
        }


        public Form1()
        {
            InitializeComponent();
            workThread = new Thread(recieveUDP);
            workThread.IsBackground = true;
            workThread.Start();
            comboBox1.SelectedIndex = 0;
            textBox1.Text = trackBar1.Value.ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Stun")
            {
                disableFreq();
                sendUDP("$FREQ,STUN,\r\n");
            }
            else if (comboBox1.SelectedItem.ToString() == "Adjustable")
            {
                enableFreq();

                string freqMSG = "$FREQ,ADJUST," + textBox1.Text.ToString() + "," + textBox2.Text.ToString() + ",\r\n";
                sendUDP(freqMSG);
            }
            else {
                disableFreq();
                sendUDP("$FREQ,KILL,\r\n");
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            textBox1.Text = trackBar1.Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Visible = false;
            sendUDP("$RESET\r\n");
            sendUDP("$RESET\r\n");
        }

        private void sendAdjustFreq() {
            trackBar1.Value = Convert.ToInt32(textBox1.Text);
            string freqMSG = "$FREQ,ADJUST," + textBox1.Text.ToString() + "," + textBox2.Text.ToString() + ",\r\n";
            sendUDP(freqMSG);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(int.Parse(textBox1.Text) >= 10){
                sendAdjustFreq();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            sendUDP("$EMPTY\r\n");
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                if (int.Parse(textBox2.Text) >= 0)
                {
                    sendAdjustFreq();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
