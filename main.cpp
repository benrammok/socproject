#include "mbed.h"
#include "rtos.h"
#include "DS18B20.h"
#include "TextLCD.h"
#include "Servo.h"
#include "ultrasonic.h"

InterruptIn resetBtn(PB_14);
InterruptIn faultBtn(PB_15);
InterruptIn emptyBtn(PB_1);
InterruptIn authBtn(USER_BUTTON);
PwmOut coil(PA_8);
TextLCD lcd(PA_7,PB_6,PC_7,PB_4,PB_5,PB_3,PA_10);
Timeout timeout;
Timeout timeout2;
Servo servo(PC_8);
DS18B20 tempSensor(PC_0, DS18B20::RES_9_BIT);

//This only purpose of this line is to
//change the baud rate of the standard usb connection
Serial pc(USBTX, USBRX, 115200);

//Prototype for the function to be called when the distance
//of the Ultrasound sensor changes
void activateCoil(int);
ultrasonic uSensor(PC_1, PB_0, .1, .1, &activateCoil);

//Periods for kill, stun, and adjustable + duration
static int killPeriod = 10; //Milliseconds
static int stunPeriod = 1000;
static int adjustablePeriod = 0;
static float duration = 1.0f;

//Variables for holding the summation of degrees each day
//as well as the limit before "Empty warning"
static int dognGrader = 0;
static int dognGradGrense = 140;

//Declare static variables for use in the program
static int itter = 10;
static int itter2 = 0;

//Variables for holding button states
static int resetPressed = 0;
static int faultPressed = 0;
static int emptyPressed = 0;
static int authPressed = 0;

//Variable for holding the ammount of rats being caught
static int numRat = 0;

//Variables for activation check for coil
static int activated = 0;


static int auth = 0;

static float temp = 0;
//Integer for holding the registred distance value and then ensure that we don't get multiple readings
static int regVal = 1000;

void resetSys()
{
    //1. If faults is present reset them
    //2. Revoke authentication
    //3. Reset variables
    //4. Reset LCD display

    faultPressed = 0;
    auth = false;
    numRat = 0;
    resetPressed = 1;
    dognGrader = 0;
    lcd.lcdComand(0x01);
}

void emptyTrap()
{
    //Reset the number of rats and refresh the lcd display
    numRat = 0;
    dognGrader = 0;
    emptyPressed = 1;
    lcd.lcdComand(0x01);
}

//Function for reading temperature from sensor
float readTemperature()
{
    temp = tempSensor.GetTemperature();
    return temp;
}

//IRQ handler for reset button
void reset()
{
    //Check if user is authenticated
    if(auth) {
        resetSys();
    }
}

//IRQ Handler for empty buton
void empty()
{
    //Check if user is authenticated
    if(auth) {
        emptyTrap();
    }
}

//IRQ Handler for fault Button
void fault()
{
    faultPressed = 1;
}


//IRQ handler for auth button
void authUser()
{
    authPressed = 1;
}

//Function for checking buttons and interactions
//This runs as a thread
void checkButtons()
{
    char buf[16];
    while(true) {
        float temp = readTemperature();

        if(resetPressed) {
            sprintf(buf, "$RESET\r");
            puts(buf);
            resetSys();
            resetPressed = 0;
        }

        if(faultPressed) {
            sprintf(buf,"$FAULT,\r");
            puts(buf);
            faultPressed = 0;
        }

        if(emptyPressed) {
            sprintf(buf,"$EMPTY\r");
            puts(buf);
            emptyPressed = 0;
        }

        if(authPressed) {
            sprintf(buf,"$AUSER\r");
            puts(buf);
            authPressed = 0;
        }
        
        //Control structure for checking if an object has passed the Ultrasound sensor
        //or if it is still present in front of the sensor, this is to prevent double readings
        int sensorVal =uSensor.getCurrentDistance();
        if(abs(sensorVal-regVal)>300 && uSensor.getCurrentDistance()<1800 && activated) {
            activated = 0;
            regVal = 1000;
        }
        
        //We don't need a lot of temp readings so one reading every second should suffice
        if(itter > 9) {
            float temp = readTemperature();
            
            sprintf(buf,"$TEMP, %3.2f\r", temp);
            puts(buf);
            
            //Start to register the daily temp if there has been detected
            //a rat present in the trap
            if(numRat > 0 && dognGrader < dognGradGrense) {
                dognGrader += temp;
            }
            
            //If the summation of temperature over the last days is over the limit
            //warn the user to empty the trap
            if(dognGrader >= dognGradGrense) {
                sprintf(buf, "$WARN,$TEMP\r");
                puts(buf);
            }
            
            //Send information about temperature and the number of rats in the system
            sprintf(buf, "$NUM, %d\r", numRat);
            puts(buf);
            sprintf(buf, "$TEMP,$DOGN, %d\r", dognGrader);
            puts(buf);
            itter = 0;


        }
        
        //For every half second update the LCD display
        if(itter == 4) {
            if(dognGrader  < dognGradGrense) {
                lcd.gotoxy(1,1);
                lcd.printf("T: %2.2f, K: %d ",temp, numRat);
                lcd.gotoxy(1,2);
                lcd.printf("");
            } else {
                lcd.gotoxy(1,1);
                lcd.printf("T: %2.2f, K: %d ",temp, numRat);
                lcd.gotoxy(1,2);
                lcd.printf("Empty Trap! ");
            }
        }

        //Refresh the LCD display every 15s to remove potential error or bugs
        if(itter2 == 149) {
            lcd.lcdComand(0x01);
            itter2 = 0;
        }
        
        //Increase the itteration values
        itter += 1;
        itter2 += 1;
        //Check wheter or not a change in distance has occured 
        uSensor.checkDistance();
        Thread::wait(100);
    }
}





void resetHatch()
{
    //Reset the hatch back to inital pos
    servo.write(0);
}

//
void enableIRQ()
{
    coil = 0;
    servo.write(100);
    timeout2.attach(&resetHatch, 1);
}

void activateCoil(int num)
{
    int sensVal = uSensor.getCurrentDistance();
    if(sensVal < 100 && !activated) {

        regVal = sensVal;

        numRat += 1;
        activated = 1;
        coil = 0.5f;
        timeout.attach(&enableIRQ, duration);
    }
}



int main()
{
    DS18B20::ROM_Code_t ROM_Code;
    tempSensor.ReadROM(&ROM_Code);
    printf("$RESET\r\n");
    //Creating threads for temperature meassurment and for checking interaction
    Thread t2(checkButtons);
    t2.set_priority(osPriorityHigh);

    //This sets the period of the coil to
    //kill setting (Default)
    coil.period(killPeriod);

    //Setup the reset button as a interrupt routine
    resetBtn.mode(PullUp);
    resetBtn.fall(&reset);

    //Setup the button which reports fault
    faultBtn.mode(PullUp);
    faultBtn.fall(&fault);

    //Setup Empty Button
    emptyBtn.mode(PullUp);
    emptyBtn.fall(&empty);

    //Setup Authorize Button
    authBtn.mode(PullUp);
    authBtn.fall(&authUser);

    //Setup Ultrasound Range Sensor, which will detect when something passes
    uSensor.startUpdates();

    //Set servo to inital position
    servo.write(0);

    //Character buffer for storing recieved data
    char buffer[80];
    char *savePnt;
    //osThreadSetPriority(osThreadGetId(), osPriorityRealtime);


    while (true) {

        //Line for reading in data recived trough stdio
        scanf("%[^\r\n]s", buffer);
        getchar();

        char* csvTok;

        csvTok = strtok_r(buffer, ",", &savePnt);

        //Controll structure for Reset function
        if(strcmp(csvTok, "$RESET") == 0) {
            //Reset the system if command is recieved
            resetSys();
        }

        //Empty sructure
        if(strcmp(csvTok, "$EMPTY") == 0) {
            emptyTrap();
        }

        //Authenticate User
        if(strcmp(csvTok, "$AUTH") == 0) {
            auth = 1;
        }


        //Controll structure for frequency adjustment
        if(strcmp(csvTok, "$FREQ") == 0) {

            //Next token
            csvTok = strtok_r(NULL, ",", &savePnt);

            //Make sure the csvTok is not empty
            if(csvTok != NULL) {
                if(strcmp(csvTok, "STUN")==0) {
                    coil.period_ms(stunPeriod);
                    duration = 5.0f;
                    //Controll statement for the adjustment of freq and delay
                } else if(strcmp(csvTok, "ADJUST")==0) {
                    csvTok = strtok_r(NULL, ",", &savePnt);
                    if(csvTok != NULL) {
                        adjustablePeriod = (atoi(csvTok));
                        float periodMS = (float)1/adjustablePeriod;
                        coil.period(periodMS);

                        csvTok = strtok_r(NULL, ",", &savePnt);

                        float durTime = (atoi(csvTok));

                        if(durTime != 0 || durTime <= 20000) {
                            duration = (durTime / 1000);
                        }

                    } else {
                        adjustablePeriod = 0;
                    }
                } else {
                    //We don't need to process the kill command
                    //as it is the normal setting;
                    coil.period_ms(killPeriod);
                    duration = 1.0f;
                }
            }
        }
    }
}