from SimpleCV import Camera, Barcode
import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.OUT)

def checkQR():
    camera = Camera()
    GPIO.output(21, GPIO.HIGH)
    sleep(1)
    GPIO.output(21, GPIO.LOW)
    sleep(1)
    GPIO.output(21, GPIO.HIGH)
    img = camera.getImage()
    GPIO.output(21, GPIO.LOW)
    barcodes = img.findBarcode()

    if barcodes is not None:
        for barcode in barcodes:
            if(barcode.data == "authorize"):
                print("$AUTH")
            else:
                print("$ERR")
    else:
        print("No QR detected!")

    sleep(1)
    GPIO.output(21, GPIO.LOW)

if __name__ == "__main__":
    checkQR()
