# README #

This repository contains the code for a C# program, Node-red flow, Python code as well as Microcontroller program for the prototype of a rat trap. Developed in the course ENSOC3001.
The python code and node-red are intended to be runned on a Raspberry pi.
Microcontroller used is the STM NUCLEO-F303RE. 
The main.cpp is present in the repository, but lacks the nessecary libraries.
To test the program you can find and import it from the link bellow
<a href="https://os.mbed.com/users/benrammok/code/SocProject/">https://os.mbed.com/users/benrammok/code/SocProject/</a>

# NOTICE #
There is some issues with the DS18B20-1wire library. 
If you import the library from mbed, you will have to change the variable <b>CRC</b> in the library to <b>crc</b> to be able to compile properly.

To get data to and from the C# program the IP Address under the SendUDP function must be changed to the IP Adress of the Pi hosting Node-Red